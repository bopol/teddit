# Changelog
## 2021-01-23
### Fixed
1. Styling of footer (PR: #132)
2. Fix (#130) - Placement of buttons.
## 2021-01-22
### Fixed
1. Fix short comment URLs(#114)
2. Fix unescape's regex(#128)
3. Optimize CSS for narrow devices(#123)
## 2021-01-21
### Fixed
1. Styling of sepia theme
## 2021-01-20
### Added
1. Added Teddit logo to the topbar
2. Sepia theme
     - Teddit has a new beautiful Sepia theme. Go to preferences to change the theme.
## 2021-01-19
### Added
1. Expand text posts from subreddit view
    - Now you can expand/preview text posts by clicking on the hamburger icon.
### Fixed
1. Check that Gallery ID exists
2. Optimized CSS
## 2021-01-17
### Added
1. Support for r/random
2. add '/subreddits'
    - Now you can search and explore subreddits easily. Add '/subreddits' to the URL or press the **more** button in the top bar.
## 2021-01-16
### Added
- Convert reddit.com links to instance domain
## 2021-01-15
### Added
-  scroll to infobar when viewing comment permalink (#78) 
### Fixed
- Fix sidebar overflow on mobile (#109)
## 2021-01-12
### Added
- Added r/popular to list of subreddits
## 2021-01-10
### Added
- Edit date for comments
### Fixed
- Position of subscribe button in mobile
- Inconsistency of Link colours
## 2021-01-09
### Added
- User info on top of entries
- r/all even when users have subscriptions
### Fixed
- Previous/Next links on page.
## 2021-01-08
### Added
- Subscribe to subreddits and manage subscriptions from preferences page.
### Fixed
- Fixed subreddit view when there are no subscriptions.








